function getModifiedArray(data) {
	return data.map(function(item){
		return {
			url:'http://'+ item.url,
			name:capitalizeFirstLetter(item.name.toLowerCase()),
			params:item.params.status+'=>'+item.params.progress,
			description:item.description.substring(0, 15) + '...',
			date:timestampToDate(item.date),
		}
	})
}

function capitalizeFirstLetter(string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
}

function timestampToDate(timestamp) {
    var tmpDate = new Date(timestamp),
        year = tmpDate.getFullYear(),
        month = tmpDate.getMonth() + 1,
        day = tmpDate.getDate(),
        hours = tmpDate.getHours(),
        minutes = tmpDate.getMinutes();
    if (month < 10) {
        month = "0" + month;
    }
    if (day < 10) {
        day = "0" + day;
    }
    if (hours < 10) {
        hours = "0" + hours;
    }
    if (minutes < 10) {
        minutes = "0" + minutes;
    }
    return year +'/'+ month+'/'+day +' '+ hours +':'+ minutes;
}

function firstQueueItems(item){
    var first = document.querySelector('#first-line');
    var resultHTML = "";
    var itemTemplate = '<div class="col-sm-3 col-xs-6">\
                    <img src="$url" alt="$name" class="img-thumbnail">\
                    <div class="info-wrapper">\
                        <div class="text-muted">$name</div>\
                        <div class="text-muted">$description</div>\
                        <div class="text-muted">$params</div>\
                        <div class="text-muted">$date</div>\
                    </div>\
                </div>';

        resultHTML = itemTemplate
            .replace(/\$name/gi, item.name)
            .replace("$url", item.url)
            .replace("$params", item.params)
            .replace("$description", item.description)
            .replace("$date", item.date);
    first.innerHTML += resultHTML;
}

function secondQueueItems(item){
    var second = document.querySelector('#second-line');
    var resultHTML = "";
    var itemTemplate = `<div class="col-sm-3 col-xs-6">\
                        <img src="${item.url}" alt="${item.name}" class="img-thumbnail">\
                        <div class="info-wrapper">\
                            <div class="text-muted">${item.name}</div>\
                            <div class="text-muted">${item.description}</div>\
                            <div class="text-muted">${item.params}</div>\
                            <div class="text-muted">${item.date}</div>\
                        </div>\
                    </div>`;

    second.innerHTML += itemTemplate;
}

function lastQueueItems(item) {
    var third = document.getElementById('third-line');
    var elemDiv = document.createElement("div");
    elemDiv.className = "col-sm-3 col-xs-6";
    third.appendChild(elemDiv);

    var elemImg = document.createElement("img");
    elemImg.setAttribute('src', item.url);
    elemImg.className = "img-thumbnail";

    elemImg.setAttribute('alt', item.name);
    elemDiv.appendChild(elemImg);

    var divDescription = document.createElement('div');
    divDescription.className = "info-wrapper";
    elemDiv.appendChild(divDescription);

    var name = document.createElement("div");
    name.setAttribute("class", "text-muted");
    name.innerHTML = item.name;

    var description = document.createElement("div");
    description.setAttribute("class", "text-muted");
    description.innerHTML = item.description;

    var params = document.createElement("div");
    params.setAttribute("class", "text-muted");
    params.innerHTML = item.params;

    var date = document.createElement("div");
    date.setAttribute("class", "text-muted");
    date.innerHTML = item.date;

    divDescription.appendChild(name);
    divDescription.appendChild(description);
    divDescription.appendChild(params);
    divDescription.appendChild(date);
}

function transform() {
	var result = getModifiedArray(data);

    result.forEach(function(item, index){
        if(index<=2){
            firstQueueItems(item);
        } else if(index<=5) {
            secondQueueItems(item);
        } else if(index<9) {
            lastQueueItems(item);
        }
	})
}

transform();
