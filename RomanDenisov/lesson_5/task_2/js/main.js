//Второе задание по 5 уроку

"use strict";

let btn = document.getElementById("play");
let player1 = document.getElementById("player1");
let player2 = document.getElementById("player2");
let result = document.getElementById("result");

let getPlayerResult = () => {
    return Math.floor((Math.random() * 3) + 1);
};

let getNameById = (number) => {
    let nameNumbers = ["", "Камень", "Ножницы", "Бумага"];
    return nameNumbers[number];
};

let determineWinner = (first, second) => {
    if (first == second) {
        return 3;
    }
    if (first == 1 && second == 2 ||
        first == 2 && second == 3 || first == 3 && second == 1) {
        return 1;
    } else if (first == 2 && second == 1) {
        return 2
    } else {
        return 2;
    }
};

let printResult = (winnerNumber) => {    
    switch(winnerNumber){
        case 1:
            result.innerHTML = "Выиграл первый игрок!";
            break;
        case 2:
            result.innerHTML = "Выиграл второй игрок!";
            break;
        case 3:
            result.innerHTML = "Ничья.Надо переиграть!";
            break;
    }
};

let runGame = () => {
    let numberOne  = getPlayerResult();
    let numberSecond = getPlayerResult();
    player1.innerHTML = getNameById(numberOne);
    player2.innerHTML = getNameById(numberSecond);
    let winnerNumber = determineWinner(numberOne,numberSecond);    
    printResult(winnerNumber);
};

btn.addEventListener("click", runGame);
