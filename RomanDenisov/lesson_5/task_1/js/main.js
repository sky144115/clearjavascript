//Домашняя работа по  уроку 5

"use strict";

 var placeTextResult = document.getElementById("result");


let randomNumber = () => {
   return  Math.floor((Math.random() * 6) + 1);
};

let printResult = (elementId, textForPrint) => {
   elementId.innerHTML += textForPrint;
};

let isNumbersEqual = (first, second) => {
      if(first == second) {
         printResult(placeTextResult, `Выпал дубль.Число ${first}<br>`);
      }
};

let isBigDifference = (first, second) => {
    if(first <= 3 && second >= 3) {
        printResult(placeTextResult, `Большой разброс между костями.
                             Разница составляет ${second-first}<br>`);
    }
};

let calcTotalResult = (result) => {
    return result > 100 ? printResult(placeTextResult, `Победа, Вы набрали ${total} очков!`) :
        printResult(placeTextResult, `Вы проиграли, у Вас ${total} очков`);
};

let run = () => {
    let  total  = 0;
    for (let i = 1; i < 16; i++) {
        let first = randomNumber();
        let second = randomNumber();
        if( i == 8 || i == 13) {continue};
        printResult(placeTextResult, `Первая кость: ${first} -- Вторая кость: ${second}<br>`);
        isNumbersEqual(first, second);
        isBigDifference(first, second);
        total += first + second;
    }

    calcTotalResult(total);
};

run();