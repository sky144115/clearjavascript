var total = 0;
var resultArea = document.getElementById("result");
for(var i = 1; i<= 15; i++){
    if(i == 8 || i == 13) {continue;}
    var first = Math.floor((Math.random() * 6) + 1);
    var second = Math.floor((Math.random() * 6) + 1);
    resultArea.innerHTML += "<div class='result-of-throw'> Первая кость: "+ "<span class='dice'>"+ first +"</span>"+" Вторая кость: " +"<span class='dice'>" + second + "</span>"+ "</div>";
    if(first === second){
        resultArea.innerHTML += "<div class='result-with-feature'>Выпал дубль. Число " + first + "</div>";
    } else if( first < 3 && second > 4) {
        resultArea.innerHTML += "<div class='result-with-feature'>Большой разброс между костями. Разница составляет " + (second - first) + "</div>";
    }
    total += first + second;
}
resultArea.innerHTML += (total > 100) ? "<div class='result-of-game'>Победа, вы набрали " + total+ " очков</div>": "<div class='result-of-game'>Вы проиграли, у вас " + total + " очков</div>";